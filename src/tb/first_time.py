import glob
import os
from importlib import reload
from os.path import join, expanduser, isfile, isdir

import yaml
from cement.utils import shell
from prompt_toolkit.validation import ValidationError, Validator

from tb.config import configured_org_repo_dir, get_default_config_tb_path
from tb.core.exc import TbError
from tb.term import ColoredTerminal
import sys


class GitUrlValidator(Validator):

    def __init__(self, term):
        self._term = term

    def validate(self, document):
        url = document.text
        if url:
            self._term.action("\nValidating", url)
            out, err, code = shell.cmd(f'git ls-remote {url}')
            if code:
                raise ValidationError(message='Git url is either invalid or not accessible')
        else:
            raise ValidationError(message='Git url is required')


def ensure_user_tb_config():

    if '-v' in sys.argv or '--version' in sys.argv:
        return

    default_config = read_config(ColoredTerminal(theme={}), get_default_config_tb_path())
    term = ColoredTerminal(theme=default_config.get('tb', {}).get('theme', {}))

    try:
        config = read_user_config(term)
    except TbError:
        config = {'tb': {}}

    if 'src_dir' not in config['tb']:
        term.warn("No projects source directory defined.")
        src = term.prompt("Into which directory do you want the repositories cloned?", default=os.getcwd())
        config['tb']['src_dir'] = src
        write_config(config)

    if 'org_repo' not in config['tb']:
        term.warn("No organization configuration found.")
        org_repo = term.prompt("What git repository contains your organization's team and repository configuration?",
                               validator=GitUrlValidator(term), validate_while_typing=False,
                               default="git@bitbucket.org:atlassian/tb-atlassian.git")

        config['tb']['org_repo'] = org_repo
        write_config(config)

        pull_all_repositories(term)

    if 'teams' not in config['tb']:
        config_dir = configured_org_repo_dir(config)

        all_teams = []
        for config_file in [join(config_dir, f) for f in glob.glob1(config_dir, "*.yml")]:
            with open(config_file) as cf:
                all_teams += yaml.load(cf.read()).get('teams', {}).keys()

        term.info('\nChoose which team repositories to track:')
        teams_input = []
        for team in all_teams:
            if term.yesno("Do you want to track {} repositories?".format(team)):
                teams_input.append(team)
        if teams_input:
            term.info("To clone the chosen repositories, run 'tb repo sync'")

        config['tb']['teams'] = teams_input
        write_config(config)
        pull_all_repositories(term)
        term.h1("Teams and repositories cloned successfully")


def read_user_config(term=None):
    if not term:
        term = ColoredTerminal(theme={})

    proper_config_path = join(expanduser("~"), '.tb', 'tb.yml')
    old_config_path = join(expanduser("~"), '.tb.yml')
    tb_config = proper_config_path
    migrate = False
    if not isfile(tb_config) and isfile(old_config_path):
        migrate = True
        tb_config = join(expanduser("~"), '.tb.yml')

    config = read_config(term, tb_config)

    if migrate:
        term.action("Migrating configuration to", proper_config_path)
        write_config(config)
        # os.remove(tb_config)
    return config


def read_config(term, path):
    if isfile(path):
        with open(path, 'r') as f:
            try:
                return yaml.load(f)
            except yaml.YAMLError as exc:
                term.error(exc)
    return {'tb': {}}


def pull_all_repositories(term):
    import tb.main
    reload(tb.main)
    with tb.main.TbConfigured(argv=['repo', 'sync']) as app:
        app.run()
        if app.exit_code != 0:
            term.error("Cannot pull repository")
            exit(1)


def write_config(config):
    config_dir = os.path.join(os.path.expanduser('~'), '.tb')
    if not isdir(config_dir):
        os.mkdir(config_dir)

    config_file = os.path.join(os.path.expanduser('~'), '.tb', 'tb.yml')
    with open(config_file, 'w') as outfile:
        yaml.dump(config, outfile, default_flow_style=False)
    print("\n{} saved\n".format(config_file))
